'use strict';

let array = [ 35, 4, 16, 4, 65, 1, 64, 1, 43, 3, 10, 31, 5 ,44 ,6, 14, 46, 54, 48, 51, 27, 84, 94 ];

function insertionSort(array) {
  let length = array.length;
  for(let i = 0; i < length; i++) {
    let temp = array[i];
    for(var j = i - 1; j >= 0 && array[j] > temp; j--) {
      array[j + 1] = array[j]; 
    }
    array[j + 1] = temp;
  }
  return array;
}

console.log(array);
insertionSort(array);
console.log(array);

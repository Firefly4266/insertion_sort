'use strict';

let array = [ 35, 4, 16, 4, 65, 1, 64, 1, 43, 3, 10, 31, 5 ,44 ,6, 14, 46, 54, 48, 51, 27, 84, 94 ];

//<-----------------------insertion sort---------------------->

/* let's start with our function declaration */
function insertionSort(array) {

  /* next we set a length variable */
  let length = array.length;

  /* now we run our first `for` loop */
  for(let i = 0; i < length; i++) {

    /* now that we have defined i we set the value of array[i] to a temporary variable so that the value at array[i] can be reassigned if necessary */
    let temp = array[i];

    /* next we run our second loop which we will need to compare values.  we set this loop to be one 'behind' the first loop in order to compare the two values. if the value in the 'j' loop is greater than the temp value (which was set from the 'i loop') we set the value from the 'j' loop to to the 'i' loop position (where we got the temp value from) */
    for(var j = i -1; j >= 0 && array[j] > temp; j--) {

      /* if both conditions are true we set the value at array[j] to the array[j = 1] position since it is a larger value then the the one to its right.  the value at [j + 1] can be replaced because we saved it in the temp variable */
      array[j + 1] = array[j];
    }

    /* if none of the conditionale are satisified then the temp variable wiil be set to the [j + 1] position. */
    array[j + 1] = temp;
  }

  /* and of course don't forget to return the array' */
  return array;
} 

console.log(array);
insertionSort(array);
console.log(array);